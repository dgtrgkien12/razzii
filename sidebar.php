<?php

/**
 * The sidebar containing the main widget area
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Razzii
 */
?>

<?php if( is_shop()){
    ?>
    <div id="sidebar-shop" class="sidebar">
        <?php dynamic_sidebar( 'shop-sidebar' ); ?>
    </div>
    <?php
}