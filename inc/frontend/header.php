<?php
/**
 * Custom functions that act on header templates
 *
 * @package Razzii
 */

 /**
 * Display header
 */
function site_header() {
	get_template_part( 'template-parts/header/site-header' );
}

add_action( 'razzii_header', 'site_header', 10 );


 /**
 * Display page-header
 */
function page_header() {
	if ( ! class_exists( 'WooCommerce' ) ) {
		return;
	}
	else {

		if ( !is_shop() ) {
			get_template_part( 'template-parts/pages/page-header' );
		}
	}

}

add_action( 'razzii_header', 'page_header', 20 );