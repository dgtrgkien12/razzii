<?php
/**
 * Theme options functions
 *
 * @package Razzii
 */

 /**
 * Theme customizer configurations.
 * Register panels, sections, settings and theme.
 *
 * @return array
 */
function razzii_theme_options( $wp_customize ) {
	//add_panel
	$wp_customize->add_panel( 'razzii_header', array(
		'title'    => esc_html__( 'Header', 'razzii' ),
		'priority' => 10,
	) );

	$wp_customize->add_panel( 'razzii_footer', array(
		'title'    => esc_html__( 'Footer', 'razzii' ),
		'priority' => 20,
	) );

	//add_section
	$wp_customize->add_section( 'custom_logo', array(
		'title'   => esc_html__( 'Custom Logo', 'razzii' ),
		'panel'   => 'razzii_header',
	) );

	$wp_customize->add_section( 'custom_topbar', array(
		'title'   => esc_html__( 'Topbar', 'razzii' ),
		'panel'   => 'razzii_header',
	) );

	$wp_customize->add_section( 'footer_main', array(
		'title'   => esc_html__( 'Footer Main', 'razzii' ),
		'panel'   => 'razzii_footer',
	) );

	//add_setting
	$wp_customize->add_setting( 'razzii_logo_type', array(
		'type'              => 'theme_mod',
		'sanitize_callback' => 'sanitize_text_field',
		'label'             => esc_html__( 'Logo Type', 'razzii' ),
		'default'           => '',
	) );

	$wp_customize->add_control( 'razzii_logo_type', array(
		'type'            => 'radio',
		'section'         => 'custom_logo',
		'label'           => esc_html__( 'Logo Type', 'razzii' ),
		'choices' => array(
			'image' => esc_html__( 'Image', 'razzii' ),
			'text'  => esc_html__( 'Text', 'razzii' ),
			'svg'   => esc_html__( 'SVG', 'razzii' ),
		),
	) );

	$wp_customize->add_setting( 'razzii_logo_text', array(
		'type'              => 'theme_mod',
		'sanitize_callback' => 'sanitize_text_field',
		'label'             => esc_html__( 'Logo Type', 'razzii' ),
		'default'           => '',
	) );

	$wp_customize->add_control( 'razzii_logo_text', array(
		'type'            => 'text',
		'section'         => 'custom_logo',
		'label'           => esc_html__( 'Logo Text', 'razzii' ),
		'description'     => esc_html__( 'This is a logo text.' ),
		'active_callback' => function() {
			return 'text' == get_theme_mod( 'razzii_logo_type' );
		}
	) );

	$wp_customize->add_setting( 'razzii_logo_svg', array(
		'type'              => 'theme_mod',
		'sanitize_callback' => 'sanitize_text_field',
		'label'             => esc_html__( 'Logo Type', 'razzii' ),
		'default'           => '',
	) );

	$wp_customize->add_control( 'razzii_logo_svg', array(
		'type'              => '',
		'section'           => 'custom_logo',
		'sanitize_callback' => 'sanitize_text_field',
		'label'             => esc_html__( 'Logo SVG', 'razzii' ),
		'active_callback'   => function() {
			return 'svg' == get_theme_mod( 'razzii_logo_type' );
		}
	) );

	$wp_customize->add_setting( 'razzii_logo_image', array(
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'label'             => esc_html__( 'Logo Type', 'razzii' ),
		'default'           => '',
		'sanitize_callback' => 'esc_url_raw'
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'razzii_logo_image', array(
		'label'         => 'Logo Image',
		'description'   => esc_html__( 'This is a logo image.' ),
		'section'       => 'custom_logo',
		'settings'      => 'razzii_logo_image',
		'button_labels' => array(
			'select' => 'Select Logo',
			'remove' => 'Remove Logo',
			'change' => 'Change Logo',
		),
		'active_callback' => function() {
			return 'image' == get_theme_mod( 'razzii_logo_type' );
		}
	) ) );

	$wp_customize->add_setting( 'dimension_width', array(
		'type'  => 'theme_mod',
		'label' => esc_html__( 'Logo Width', 'razzii' ),
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'dimension_width', array(
		'type'     => 'number',
		'label'    => 'Width',
		'section'  => 'custom_logo',
		'settings' => 'dimension_width',
		'default'  => 'auto',
		'active_callback' => function() {
			return 'image' == get_theme_mod( 'razzii_logo_type' );
		}
	) ) );

	$wp_customize->add_setting( 'dimension_height', array(
		'type'  => 'theme_mod',
		'label' => esc_html__( 'Logo Height', 'razzii' ),
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'dimension_height', array(
		'type'     => 'number',
		'label'    => 'Height',
		'section'  => 'custom_logo',
		'settings' => 'dimension_height',
		'default'  => 'auto',
		'active_callback' => function() {
			return 'image' == get_theme_mod( 'razzii_logo_type' );
		}
	) ) );

	$wp_customize->add_setting( 'razzii_topbar', array(
		'type'    => 'theme_mod',
		// 'sanitize_callback' => 'sanitize_text_field',
		'label'   => esc_html__( 'Topbar', 'razzii' ),
		'default' =>'',
	) );

	$wp_customize->add_control( 'razzii_topbar', array(
		'type'    => 'radio',
		'section' => 'custom_topbar',
		'label'   => esc_html__( 'Topbar Type', 'razzii' ),
		'choices' => array(
			'on'  => esc_html__( 'On Topbar', 'razzii' ),
			'off' => esc_html__( 'Off Topbar', 'razzii' ),
		),
	) );

	$wp_customize->add_setting( 'topbar_left_item', array(
		'type'    => 'theme_mod',
		// 'sanitize_callback' => 'sanitize_text_field',
		'label'   => esc_html__( 'Topbar Left', 'razzii' ),
		'defauls' => '',
	) );

	$wp_customize->add_control( 'topbar_left_item', array(
		'type'    => 'select',
		'section' => 'custom_topbar',
		'label'   => esc_html__( 'Topbar Left', 'razzii' ),
		'choices' => array(
			'menu_social' => esc_html__( 'Menu Social', 'razzii' ),
			'menu_topbar' => esc_html__( 'Menu Topbar', 'razzii' ),
			'none'        => esc_html__( 'None', 'razzii' ),
		),
		'active_callback' => function() {
			return 'on' == get_theme_mod( 'razzii_topbar' );
		}
	) );

	$wp_customize->add_setting( 'topbar_height', array(
		'type'  => 'theme_mod',
		'label' => esc_html__( 'Topbar Height', 'razzii' ),

	) );

	$wp_customize->add_control( 'topbar_height', array(
		'type'              => 'number',
		'label'             => 'Height',
		'section'           => 'custom_topbar',
		'sanitize_callback' => 'my_sanitize_number',
		'input_attrs'       => array(
			'min' => 40,
			'max' => 200,
		),
		'active_callback' => function() {
			return 'on' == get_theme_mod( 'razzii_topbar' );
		}
	) );

	$wp_customize->add_setting( 'item-left', array(
		'type'              => 'theme_mod',
		'sanitize_callback' => 'sanitize_text_field',
		'label'             => esc_html__( 'Item Left', 'razzii' ),
		'default'           => '',
	) );

	$wp_customize->add_control( 'item-left', array(
		'type'              => 'text',
		'section'           => 'footer_main',
		'label'             => esc_html__( 'Item Left', 'razzii' ),
		'sanitize_callback' => 'sanitize_text_field',
	) );

}
add_action( 'customize_register', 'razzii_theme_options' );

// function razzii_customize_controls_enqueue_scripts() {
// 	wp_enqueue_script( 'razzii-customize-helpers', get_theme_file_uri( '/assets/js/customize.js' ), array( 'customize-base', 'customize-controls' ), wp_get_theme()->get( 'Version' ), true);
// }
// add_action( 'customize_controls_enqueue_scripts', 'razzii_customize_controls_enqueue_scripts' );
