<?php
/**
 * WooCommerce functionalities
 *
 * @package Razzii
 */

/**
 * Setup WooCommerce
 */
function razzii_woocommerce_setup() {
	add_theme_support( 'woocommerce', array(
		'product_grid'          => array(
		  'min-rows'   => 5,
		  'max-rows'   => 10,
		  'min-colums' => 3,
		  'max-colums' => 3,
		),
	) );

	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );

	add_filter( 'woocommerce_enqueue_styles', '__return_false' );
}

add_action( 'after_setup_theme', 'razzii_woocommerce_setup' );

/**
 * Init widgets & sidebars for WooCommerce
 */
function razzii_woocommerce_init_widgets() {
	register_sidebar( array(
		'name'          => esc_html__( 'Shop Sidebar', 'razzii' ),
		'id'            => 'shop-sidebar',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

add_action( 'widgets_init', 'razzii_woocommerce_init_widgets' );

/**
 * Enqueue WC Script
 */
function razzii_woocommerce_enqueue_scripts() {
	wp_enqueue_style( 'razzii-woocommerce-style', get_theme_file_uri( '/woocommerce.css' ) );
}

add_action( 'wp_enqueue_scripts', 'razzii_woocommerce_enqueue_scripts', 20 );



require get_theme_file_path( '/inc/woocommerce/razzii-woo-template-product.php' );
require get_theme_file_path( '/inc/woocommerce/razzii-woo-template-product-loop.php' );
require get_theme_file_path( '/inc/woocommerce/razzii-woo-template-product-summary.php' );
require get_theme_file_path( '/inc/woocommerce/razzii-woo-template-single-product.php' );
require get_theme_file_path( '/inc/woocommerce/razzii-woo-helper.php' );
require get_theme_file_path( '/inc/woocommerce/razzii-woo-template-cart.php' );
