<?php

/**
 *Display single product
 *
 * @package Razzii
 */

//Remove Sale flash
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);

/**
 * Star rating HTML.
 *
 * @since 1.0.0
 *
 * @param string $html Star rating HTML.
 * @param int $rating Rating value.
 * @param int $count Rated count.
 *
 * @return string
 */
function star_rating_html($html, $rating, $count)
{
	$html = '<span class="max-rating rating-stars">'
		. '<span class="rz-icon-svg">' . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>' . '</span>'
		. '<span class="rz-icon-svg">' . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>' . '</span>'
		. '<span class="rz-icon-svg">' . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>' . '</span>'
		. '<span class="rz-icon-svg">' . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>' . '</span>'
		. '<span class="rz-icon-svg">' . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>' . '</span>'
		. '</span>';
	$html .= '<span class="user-rating rating-stars" style="width:' . (($rating / 5) * 100) . '%">'
		. '<span class="rz-icon-svg">' . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>' . '</span>'
		. '<span class="rz-icon-svg">' . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>' . '</span>'
		. '<span class="rz-icon-svg">' . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>' . '</span>'
		. '<span class="rz-icon-svg">' . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>' . '</span>'
		. '<span class="rz-icon-svg">' . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>' . '</span>'

		. '</span>';

	$html .= '<span class="screen-reader-text">';

	if (0 < $count) {
		/* translators: 1: rating 2: rating count */
		$html .= sprintf(_n('Rated %1$s out of 5 based on %2$s customer rating', 'Rated %1$s out of 5 based on %2$s customer ratings', $count, 'razzi'), '<strong class="rating">' . esc_html($rating) . '</strong>', '<span class="rating">' . esc_html($count) . '</span>');
	} else {
		/* translators: %s: rating */
		$html .= sprintf(esc_html__('Rated %s out of 5', 'razzi'), '<strong class="rating">' . esc_html($rating) . '</strong>');
	}

	$html .= '</span>';

	return $html;
}

// Change star rating HTML.
add_filter('woocommerce_get_star_rating_html', 'star_rating_html', 10, 3);


/**
 * Add class product-gallery-summary
 *
 *
 */
function razzii_add_class_product_gallery_summary()
{
	echo '<div class="product-gallery-summary">';
}

add_action('woocommerce_before_single_product_summary', 'razzii_add_class_product_gallery_summary', 1);

/**
 * End class product-gallery-summary
 *
 *
 */
function razzii_end_class_product_gallery_summary()
{
	echo '</div>';
}

add_action('woocommerce_after_single_product_summary', 'razzii_end_class_product_gallery_summary', 10);


//Change single rating
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 5);

//Change single title
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 7);


//Change output_product_data_tabs
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
add_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 20);

//Change upsell_display
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
add_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 30);

//Change related_products
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
add_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 40);

//Remove woocommerce_single_variation
remove_action('woocommerce_single_variation', 'woocommerce_single_variation');


/**
 * Reset variations
 *
 */
function rz_reset_variations()
{
	return sprintf(
		'<a class="reset_variations" href="#">
		<span class="rz-icon-svg"><img src="' . esc_url(get_theme_file_uri('assets/images/close.svg')) . '"></span>
		</a>',
	);
}
add_filter('woocommerce_reset_variations_link', 'rz_reset_variations');

/**
 * Add class product-button-wrapper
 *
 */
function razzii_product_button_wrapper()
{
	echo '<div class="product-button-wrapper">';
}

add_action('woocommerce_before_add_to_cart_button', 'razzii_product_button_wrapper');

/**
 * End class product-button-wrapper
 *
 */
function razzii_end_product_button_wrapper()
{
	echo '</div>';
}

add_action('woocommerce_after_add_to_cart_button', 'razzii_end_product_button_wrapper');


/**
 * Product Share
 *
 */
function product_share()
{
?>
	<div class="product-share share">
		<span class="sharing-icon">
			<?php esc_html_e('Share:', 'razzii') ?>
		</span>
		<span class="socials">
			<a href="#" target="_blank" class="social-share-link facebook"><span class="rz-icon-svg"><svg aria-hidden="true" role="img" focusable="false" width="24" height="24" viewBox="0 0 7 12" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M5.27972 1.99219H6.30215V0.084375C6.12609 0.0585937 5.51942 0 4.81306 0C3.33882 0 2.32912 0.99375 2.32912 2.81953V4.5H0.702148V6.63281H2.32912V12H4.32306V6.63281H5.88427L6.13245 4.5H4.32306V3.03047C4.32306 2.41406 4.47791 1.99219 5.27972 1.99219Z"></path></svg></span><span class="after-text">Share on Facebook</span></a>
			<a href="#" target="_blank" class="social-share-link twitter"><span class="rz-icon-svg"><svg aria-hidden="true" role="img" focusable="false" viewBox="0 0 24 24" width="24" height="24" fill="currentColor"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg></span><span class="after-text">Share on Twitter</span></a>
			<a href="#" target="_blank" class="social-share-link pinterest"><span class="rz-icon-svg"><svg aria-hidden="true" role="img" focusable="false" width="24" height="24" fill="currentColor" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M12.289,2C6.617,2,3.606,5.648,3.606,9.622c0,1.846,1.025,4.146,2.666,4.878c0.25,0.111,0.381,0.063,0.439-0.169 c0.044-0.175,0.267-1.029,0.365-1.428c0.032-0.128,0.017-0.237-0.091-0.362C6.445,11.911,6.01,10.75,6.01,9.668 c0-2.777,2.194-5.464,5.933-5.464c3.23,0,5.49,2.108,5.49,5.122c0,3.407-1.794,5.768-4.13,5.768c-1.291,0-2.257-1.021-1.948-2.277 c0.372-1.495,1.089-3.112,1.089-4.191c0-0.967-0.542-1.775-1.663-1.775c-1.319,0-2.379,1.309-2.379,3.059 c0,1.115,0.394,1.869,0.394,1.869s-1.302,5.279-1.54,6.261c-0.405,1.666,0.053,4.368,0.094,4.604 c0.021,0.126,0.167,0.169,0.25,0.063c0.129-0.165,1.699-2.419,2.142-4.051c0.158-0.59,0.817-2.995,0.817-2.995 c0.43,0.784,1.681,1.446,3.013,1.446c3.963,0,6.822-3.494,6.822-7.833C20.394,5.112,16.849,2,12.289,2"></path></svg></span><span class="after-text">Share on Pinterest</span></a>
		</span>
	</div>
<?php
}

add_action( 'woocommerce_share','product_share', 10 );
