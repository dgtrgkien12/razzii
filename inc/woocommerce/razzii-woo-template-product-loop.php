<?php

/**
 * Add class product thumbnail
 *
 */
function product_loop_thumbail_open()
{
	echo '<div class="product-thumbnail">';
}

add_action('woocommerce_before_shop_loop_item_title', 'product_loop_thumbail_open', 5 );

/**
 * Close class product thumbnail
 *
 *
 */
function product_loop_thumbail_close()
{
	echo '</div>';;
}

add_action('woocommerce_before_shop_loop_item_title', 'product_loop_thumbail_close', 40 );

// add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 10 );

/**
 * Add class product loop button
 */
function product_loop_button_open()
{
	echo '<div class="product-loop-button">';
}

add_action('woocommerce_before_shop_loop_item_title', 'product_loop_button_open', 20 );

/**
 * Close class product thumbnail
 */
function product_loop_button_close()
{
	echo '</div>';;
}

add_action('woocommerce_before_shop_loop_item_title', 'product_loop_button_close', 30 );

/**
 * Add to cart
 *
 *
 */
function add_to_cart_link()
{
	woocommerce_template_loop_add_to_cart();
	echo '<a href="#"><span class="rz-icon-svg add-to-cart-text loop_button-text"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg></span></a>';
	echo '<a href="#"><span class="rz-icon-svg add-to-cart-text loop_button-text"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" ><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></span></a>';
}
add_action('woocommerce_before_shop_loop_item_title', 'add_to_cart_link', 25 );



add_filter('woocommerce_loop_add_to_cart_link', function ($html, $product, $args) {
	return sprintf(
		'<a href="%s" data-quantity="%s" class="%s" %s>
		<span class="rz-icon-svg">
		<svg aria-hidden="true" role="img" focusable="false" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"> <path d="M21.9353 20.0337L20.7493 8.51772C20.7003 8.0402 20.2981 7.67725 19.8181 7.67725H4.21338C3.73464 7.67725 3.33264 8.03898 3.28239 8.51523L2.06458 20.0368C1.96408 21.0424 2.29928 22.0529 2.98399 22.8097C3.66874 23.566 4.63999 24.0001 5.64897 24.0001H18.3827C19.387 24.0001 20.3492 23.5747 21.0214 22.8322C21.7031 22.081 22.0361 21.0623 21.9353 20.0337ZM19.6348 21.5748C19.3115 21.9312 18.8668 22.1275 18.3827 22.1275H5.6493C5.16836 22.1275 4.70303 21.9181 4.37252 21.553C4.042 21.1878 3.88005 20.7031 3.92749 20.2284L5.056 9.55014H18.9732L20.0724 20.2216C20.1223 20.7281 19.9666 21.2087 19.6348 21.5748Z" fill="currentColor"></path> <path d="M12.1717 0C9.21181 0 6.80365 2.40811 6.80365 5.36803V8.6138H8.67622V5.36803C8.67622 3.44053 10.2442 1.87256 12.1717 1.87256C14.0992 1.87256 15.6674 3.44053 15.6674 5.36803V8.6138H17.5397V5.36803C17.5397 2.40811 15.1316 0 12.1717 0Z" fill="currentColor"></path> </svg>
		</span>
		<span class="screen-reader-text">%s</span>
		</a>',
		esc_url($product->add_to_cart_url()),
		esc_attr(isset($args['quantity']) ? $args['quantity'] : 1),
		esc_attr(isset($args['class']) ? $args['class'] : 'button'),
		isset($args['attributes']) ? wc_implode_html_attributes($args['attributes']) : '',
		esc_html($product->add_to_cart_text())
	);
}, 10, 3);

/**
 * Add product title
 *
 */
function razzii_add_loop_product__title(){
	echo '<h2 class="woocommerce-loop-product__title">';
	woocommerce_template_loop_product_link_open();
	the_title();
	woocommerce_template_loop_product_link_close();
	echo '</h2>';
}

//Remove add cart
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

// Change wrapper link
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 5 );

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_close', 30 );


//Change product title
remove_action( 'woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title', 10 );
add_action( 'woocommerce_shop_loop_item_title','razzii_add_loop_product__title', 10 );


//Add taxonomy
add_action( 'woocommerce_shop_loop_item_title','product_taxonomy', 5 );

//Remove loop rating
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

// Change sale plash
add_filter('woocommerce_sale_flash', function ($html, $post, $product) {
	return sprintf(
		'<span class="woocommerce-badges"><span class="onsale woocommerce-badge"><span> '. esc_html__( 'Sale', 'woocommerce' ) . '</span></span></span>'
	);
}, 10, 3);