<?php

/**
 * Add product summary
 *
 */
function razzii_add_product_summary()
{
	echo '<div class="product-summary">';
}
add_action('woocommerce_before_shop_loop_item_title', 'razzii_add_product_summary', 50);

/**
 * End product summary
 *
 */
function razzii_end_product_summary()
{
	echo '</div>';
}

add_action('woocommerce_after_shop_loop_item', 'razzii_end_product_summary', 100);

/**
 * Add class summary-top-box
 *
 *
 */
function razzii_add_class_summary_top_box()
{
	echo '<div class="summary-top-box">';
}

add_action('woocommerce_single_product_summary', 'razzii_add_class_summary_top_box', 2);

/**
 * End class summary-top-box
 *
 *
 */
function razzii_end_class_summary_top_box()
{
	echo '</div>';
}

add_action('woocommerce_single_product_summary', 'razzii_end_class_summary_top_box', 6);

/**
 * Single get meta cat
 *
 */

add_action( 'woocommerce_single_product_summary','product_taxonomy', 3 );
