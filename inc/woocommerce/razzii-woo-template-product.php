<?php
/**
 * Sidebar.
 *
 * @see woocommerce_get_sidebar()
 */

// add_action( 'shop-content', 'woocommerce_get_sidebar', 10 );

/**
 * Add class before loop
 */
function wc_add_class_loop () {
    ?>
    <div class="shop-content">
        <?php do_action( 'shop-content' );?>
    <?php
}

// add_action( 'woocommerce_before_main_content', 'wc_add_class_loop');

/**
 * End class after loop
 */
function wc_close_class_loop () {
    echo '</div>';
}

// add_action( 'woocommerce_after_main_content', 'wc_close_class_loop' );

/**
 * Hide page title
 */
function razzi_wc_hide_page_title( $title ) {
	$title =false ;

	return $title;
}

add_filter( 'woocommerce_show_page_title', 'razzi_wc_hide_page_title' );


/**
 * Add class catalog
 */
function razzii_add_class_catalog_banner() {
    ?>
    <div class="catalog-header-banners swiper swiper-container">
        <?php get_template_part('template-parts/catalog/catalog-header-banners') ?>
    </div>
    <?php
}

add_action( 'woocommerce_before_shop_loop', 'razzii_add_class_catalog_banner', 10 );

/**
 * Add class catalog
 */
function razzii_add_class_catalog() {
    echo '<div class="catalog">';
}

add_action( 'woocommerce_before_shop_loop', 'razzii_add_class_catalog', 15 );

/**
 * End class catalog
 */
function razzii_end_class_catalog() {
    echo  '</div>';
}

add_action( 'woocommerce_before_shop_loop', 'razzii_end_class_catalog', 60 );

/**
 * Add class product breadcrumb
 */
function razzii_add_class_product_breadcrumb() {
    ?>
        <div class="product-breadcrumb">
        <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
    <?php
}
add_action( 'woocommerce_before_shop_loop', 'razzii_add_class_product_breadcrumb', 20 );

/**
 * End class product breadcrumb
 */
function razzii_end_class_product_breadcrumb() {

    echo '</div>';

}
add_action( 'woocommerce_before_shop_loop', 'razzii_end_class_product_breadcrumb', 40 );

//Change breadcrumb
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20 );
add_action( 'woocommerce_before_shop_loop', 'woocommerce_breadcrumb', 20 );

//Remove result count
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

//Change ordering
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
add_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 50 );

//Remove paination
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );

//Change sidebar
// remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
add_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

/**
 *
 */
function razzii_wc_load() {
    get_template_part( 'template-parts/posts/load-post' );
}
add_action( 'woocommerce_after_shop_loop', 'razzii_wc_load', 10 );