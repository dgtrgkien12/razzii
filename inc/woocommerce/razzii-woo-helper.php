<?php

/**
 * Product taxonomy
 */

function product_taxonomy($taxonomy = 'product_cat')
{
	global $product;
	$taxonomy = empty($taxonomy) ? 'product_cat' : $taxonomy;
	$terms = get_the_terms($product->get_id(), $taxonomy);

	if (!empty($terms) && !is_wp_error($terms)) {
		echo sprintf(
			'<a class="meta-cat" href="%s">%s</a>',
			esc_url(get_term_link($terms[0]), $taxonomy),
			esc_html($terms[0]->name)
		);
	}
}

add_action( 'woocommerce_single_product_summary','product_taxonomy', 3 );
