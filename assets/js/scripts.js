jQuery(document).ready(function ($) {

	// header on top
	window.onscroll = function () {
		rz_sticky()
	};
	var header = document.getElementById("site-header-1-bottom-box");
	var sticky = header.offsetTop;

	function rz_sticky() {
		if (window.pageYOffset >= sticky) {
			header.classList.add("sticky-header");
		} else {
			header.classList.remove("sticky-header");
		}
	}

	//Swiper header
	var Swipes = new Swiper('.swiper-container', {
		pagination: {
			el: ".swiper-pagination",
			clickable: true,
		},
	});

	//woocommerce-product-gallery__trigger
	$(".woocommerce-product-gallery__image").click(function () {
		$(".woocommerce-product-gallery__trigger").trigger("click");
	});


	//load more Post
	$(document.body).on('click', '#blog-previous-ajax a', function (e) {
		e.preventDefault();

		if ($(this).data('requestRunning')) {
			return;
		}

		$(this).data('requestRunning', true);

		console.log($(this));

		var $posts = $(this).closest('#primary'),
			$postList = $posts.find('.post-page-col-list'),
			$pagination = $(this).parents('.load-navigation');

		$.get(
			$(this).attr('href'),
			function (response) {
				var content = $(response).find('.post-page-col-list').children('.col-post-page'),
					$pagination_html = $(response).find('.load-navigation').html();


				$pagination.html($pagination_html);
				$postList.append(content);
				$pagination.find('a').data('requestRunning', false);
			}
		);
	})

	//load more Product
	$(document.body).on('click', '#product-previous-ajax a', function (e) {
		e.preventDefault();


		if ($(this).data('requestRunning')) {
			return;
		}

		$(this).data('requestRunning', true);



		var $products = $(this).closest('#primary'),
			$productList = $products.find('.products'),
			$pagination = $(this).parents('.load-product');

		$.get(
			$(this).attr('href'),
			function (response) {
				var content = $(response).find('.products').children('.type-product'),
					$pagination_html = $(response).find('.load-product').html();

				$pagination.html($pagination_html);
				$productList.append(content);
				$pagination.find('a').data('requestRunning', false);
			}
		);
	})

	//show password
	$('#showpassword').click(function () {
		if ($(this).parent().parent().find('.input-text').get(0).type === 'password') {
			$(this).parent().parent().find('.input-text').get(0).type = 'text';
		} else {
			$(this).parent().parent().find('.input-text').get(0).type = 'password';
		}
	})




	$('.current').click(function () {
		$(this).parent().find('.qty-option').css("display", "block");
	})

	$('.remove-item-cart').click(function () {
		$(this).parent().parent().parent().css("opacity", "0.5")
		setTimeout(() => {
			$(this).parent().parent().parent().remove()
		}, 800);
		updateCart();
	})

	$('.qty-option ul li').click(function () {
		$(this).parent().parent().parent().children().find('.value').text($(this).text());
		$('.qty-option').css("display", "none");
	})

	$('.icon-cart').click(function () {
		$('.cart-modal').addClass("open")
		$('body').css("overflow", "hidden")
	})
	//close modal
	$('.button-close').click(function () {
		$('.page-modal').removeClass("open")
		$('body').css("overflow", "scroll")
	})

	$('.off-modal-layer').click(function () {
		$('.page-modal').removeClass("open")
		$('body').css("overflow", "scroll")
	})
	//click menu mobile
	$('.icon-menu-mobile').click(function () {
		$('#mobile-menu-modal').addClass("open")
		$('body').css("overflow", "hidden")
	})
	//account click
	$('#icon-account').click(function () {
		$('.account-modal').addClass("open")
		$('body').css("overflow", "hidden")
	})
	//search click
	$('#icon-search').click(function () {
		$('.search-modal').addClass("open")
		$('body').css("overflow", "hidden")
	})
	//register click
	$('.button-register').click(function () {
		$('.form-login').css("opacity", "0")
		setTimeout(function () {
			$('.form-login').css("display", "none")
		}, 200);
		setTimeout(function () {
			$('.form-register').css("display", "block")
		}, 400);
		setTimeout(function () {
			$('.form-register').css("opacity", "1")
		}, 600);
	})
	//lost-password
	$('.lost-password').click(function () {
		$('.form-register').css("opacity", "0")
		setTimeout(function () {
			$('.form-register').css("display", "none")
		}, 200);
		setTimeout(function () {
			$('.form-login').css("display", "block")
		}, 400);
		setTimeout(function () {
			$('.form-login').css("opacity", "1")
		}, 600);
	})


	// add arrow list menu children
	var arrowmenu = `<span class="arrow-menu-children">
						<span class="rz-icon-svg">
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
								stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
								<polyline points="6 9 12 15 18 9"></polyline>
							</svg>
						</span>
					</span>`

	$(".menu-modal .menu-item-has-children").append(arrowmenu);
	$(".menu-item-has-children").children().children().css("padding-left", "10px");
	$(".menu-item-has-children").children(".sub-menu").css("display", "none");


	$(".wc-block-product-categories-list").parent().append(arrowmenu);


	$(".widget-title").append(arrowmenu);

	$('.arrow-menu-children').click(function () {

		if ($(this).hasClass('flip')) {
			$(this).parent().parent().children('.woocommerce-widget-layered-nav-list').slideDown();
			$(this).parent().children('.wc-block-product-categories-list').slideDown();
			$(this).removeClass('flip')
		} else {
			$(this).parent().parent().children('.woocommerce-widget-layered-nav-list').slideUp();
			$(this).parent().children('.wc-block-product-categories-list').slideUp();
			$(this).addClass('flip')
		}
	})


	/**
   * Handle product reviews
   */
	// reviewProduct = function () {
	// 	setTimeout(function () {
	// 		$('#respond p.stars a').prepend('<span class="razzi-svg-icon "><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>');
	// 	}, 100);
	// };

});
