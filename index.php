<?php

/**
 * The main template file
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Razzii
 */

get_header(); ?>
<?php get_template_part('template-parts/posts/blog-header') ?>
<div id="primary" class="content-area ">


	<?php
	wp_nav_menu(array(
		'theme_location'  => 'category-menu',
		'container_class' => 'category-menu-container',
	));
	?>

	<?php get_template_part('template-parts/posts/content'); ?>

</div>
<?php
get_footer();
