<?php

/**
 * Enqueue Script
 */
function razzii_scripts() {
	wp_enqueue_script( 'razzii',get_theme_file_uri( '/assets/js/scripts.js' ), array( 'jquery' ), '11122021', true );
	wp_enqueue_style( 'custom-google-fonts','https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,300;0,400;0,500;0,600;0,700;1,200;1,300;1,400;1,500;1,600;1,700' );
	wp_enqueue_style( 'font-awesome','//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
	wp_enqueue_style( 'razzii',get_theme_file_uri( 'style.css' ) );
	wp_enqueue_script( 'swiper', get_theme_file_uri( '/assets/js/plugins/swiper.min.js' ) , array( 'jquery' ), '5.3.8', true );
	wp_enqueue_script( 'jquery-quantity-dropdown', get_template_directory_uri() . '/assets/js/plugins/quantity-dropdown.js', array( 'jquery' ), '12032021', true );

}
add_action( 'wp_enqueue_scripts', 'razzii_scripts' );

/**
 * Setup theme
 */
function razzii_setup(){
	add_theme_support( 'post-thumbnails' );

	//set image size
	set_post_thumbnail_size( 600, 398, true );

	//register menu
	register_nav_menus(
	  array(
		'primary' => esc_html__( 'Primary menu', 'razzii' ),
		'secondary'  => esc_html__( 'Secondary menu', 'razzii' ),
		'social-topbar-menu'  => esc_html__( 'Social topbar menu', 'razzii' ),
		'social-post-menu'  => esc_html__( 'Social post menu', 'razzii' ),
		'topbar-menu'  => esc_html__( 'Topbar menu', 'razzii' ),
		'category-menu'  => esc_html__( 'Category menu', 'razzii' ),
		'footer-menu'  => esc_html__( 'Footer menu', 'razzii' ),
	  )
	);
}

add_action( 'after_setup_theme', 'razzii_setup' );

/**
 * Register widget
 */
function razzii_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar', 'razzii' ),
		'id'            => 'footer-sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>'
	) );
}

add_action( 'widgets_init', 'razzii_widgets_init' );


/**
 * Require file
 */
require get_template_directory() . '/inc/customize.php';
require get_template_directory() . '/inc/frontend/header.php';

/**
 * Require WooCommerce file
 */
if ( class_exists( 'WooCommerce' ) ) {
	require_once get_template_directory() . '/inc/woocommerce.php';
}