<?php
get_header();
if (have_posts()) :
?>
	<?php get_template_part('template-parts/posts/blog-header') ?>

	<?php
	wp_nav_menu(array(
		'theme_location'  => 'category-menu',
		'container_class' => 'category-menu-container',
	));
	?>

	<div class="post-page-container">
		<div class="post-page-col-list">
			<?php

			while (have_posts()) : the_post(); ?>
				<div class="post">
					<div class=entry-header>
						<div class="date-post">
							<p><?php the_time('M d',) ?></p>
						</div>
						<?php if (has_post_thumbnail()) : ?>
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
						<?php endif; ?>
					</div>
					<div class="entry-summary">
						<h4><a href="<?php the_permalink(); ?>" style="text-decoration: none;"><?php echo the_title(); ?></a></h4>
						<div class="entry-content">
							<p><?php echo wp_trim_words(get_the_content(), 23) ?></p>
						</div>
						<a class="button-normal" href="<?php the_permalink(); ?>">Read more
							<span class="rz-icon-svg"><svg class="image-post" aria-hidden="true" role="img" focusable="false" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
									<line x1="0" y1="12" x2="19" y2="12"></line>
									<polyline points="12 5 19 12 12 19"></polyline>
								</svg></span>
						</a>

					</div>
				</div>
			<?php
			endwhile; ?>
		</div>
	</div>


<?php
else :
	echo '<p>There are no posts!</p>';
endif;
// get_sidebar();
get_footer();
?>