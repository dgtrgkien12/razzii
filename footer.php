<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Razzii

 */

?>

</div><!-- container clearfix -->

</div><!-- #content-->

<footer id="site-footer" class="site-footer">
	<?php get_template_part( 'template-parts/footer/footer-sub' ); ?>
	<?php get_template_part( 'template-parts/footer/footer-narrow' ); ?>
	<?php get_template_part( 'template-parts/footer/footer-hashtag' ); ?>
</footer>

<?php get_template_part( 'template-parts/modals/cart-modal' ); ?>
<?php get_template_part( 'template-parts/modals/mobile-menu-modal' ); ?>
<?php get_template_part( 'template-parts/modals/account-modal' ); ?>
<?php get_template_part( 'template-parts/modals/search-modal' ); ?>


</div><!-- page -->

<?php wp_footer(); ?>

</body>
</html>