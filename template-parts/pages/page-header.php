<?php

/**
 * Template part for displaying posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Razzii
 */
?>
<div class="page-header contaniner">
	<div class="semi-links-container">
		<a href="<?php echo site_url('/home'); ?>">
			<p>Home</p>
		</a>
		<span class="rz-icon-svg dilimiter">
		<svg aria-hidden="true" role="img" focusable="false" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="9 18 15 12 9 6"></polyline></svg>
		</span>
		<p><?php the_title() ?></p>
	</div>
</div>