<div id="search-modal" class="search-modal page-modal">
	<div class="off-modal-layer"></div>
	<div class="search-panel page-panel">
		<div class="modal-header account-modal-header">
			<h3 class="title-modal">Search</h3>
			<a href="#" class="button-close">
				<span>
					<svg aria-hidden="true" role="img" focusable="false" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</span>
			</a>
		</div>
		<div class="modal-content search-content">
			<form action="" method="get" class="form-search">
				<div class="product-container">
					<div class="product-cat">
						<div class="product-label">
							<span>All Categories</span>
							<span>
								<svg class="svg-icon" aria-hidden="true" role="img" focusable="false" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
									<polyline points="6 9 12 15 18 9"></polyline>
								</svg>
							</span>
						</div>
						<select name="" id="product-cat-modal" class="product-cat-dd">
							<option value="">All Categories</option>
							<option value="">B</option>
							<option value="">C</option>
						</select>
					</div>

					<div class="input-search">
						<input class="input-text" type="text" placeholder="Search for item and brand" name="uname" required>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>