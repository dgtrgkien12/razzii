<div id="account-modal" class="account-modal page-modal ">
	<div class="off-modal-layer"></div>
	<div class="account-panel page-panel">
		<div class="modal-header account-modal-header">
			<h3 class="title-modal">Sign in</h3>
			<a href="#" class="button-close">
				<span>
					<svg aria-hidden="true" role="img" focusable="false" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</span>
			</a>
		</div>
		<div class="modal-content account-content">
			<form action="" method="post" class="form-login" style="display:block">

				<?php do_action('woocommerce_login_form_start'); ?>

				<p class="form-row">
					<input class="input-text" type="text" placeholder="<?php  esc_html_e( 'Enter Username', 'razzii' );?>" name="username" required>
				</p>
				<p id="form-input-password" class="form-row">
					<input id="input-password" class="input-text" type="password" placeholder="<?php  esc_html_e( 'Enter Password', 'razzii' );?>" name="password" required>
					<span class="rz-icon-svg showpassword">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
							<path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
							<circle cx="12" cy="12" r="3"></circle>
						</svg>
					</span>
				</p>

				<?php do_action( 'woocommerce_login_form' ); ?>

				<p class="form-row">
					<input class="form-checkbox" type="checkbox" checked="checked" name="remember"><?php esc_html_e( 'Remember me', 'razzii' ); ?>
					<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
					<button type="submit" class="button-account button-signin button-signin-up" name="login" value="<?php esc_attr_e( 'Sign in', 'razzi' ); ?>"><?php esc_html_e( 'Sign in', 'razzi' ); ?></button>
				</p>
				<p class="form-row">
					<span class="button-account button-register button-signin button-signin-up"><?php esc_html_e( 'Create An Account', 'razzi' ); ?></span>
				</p>
				<p class="lost-passworda">
					<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>">Forgot password?</a>
				</p>
			</form>
			<form action="" method="post" class="form-register" style="display:none;opacity:0;">
				<p class="form-row">
					<input class="input-text" type="text" placeholder="<?php  esc_html_e( 'Username', 'razzii' );?>" name="username" required>
				</p>
				<p class="form-row">
					<input class="input-text" type="text" placeholder="<?php  esc_html_e( 'Email address', 'razzii' );?>" name="emailadd" required>
				</p>
				<p id="form-input-password" class="form-row">
					<input id="input-password" class="input-text" type="password" placeholder="<?php  esc_html_e( 'Password', 'razzii' );?>" name="password" required>
					<span class="rz-icon-svg showpassword">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
							<path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
							<circle cx="12" cy="12" r="3"></circle>
						</svg>
					</span>
				</p>
				<button type="submit" class="button-account button-signin button-signin-up" name="login" value="<?php esc_attr_e( 'Sign up', 'razzi' ); ?>"><?php esc_html_e( 'Sign up', 'razzi' ); ?></button>
				<p class="lost-password">
					<a href="#">Already has a account</a>
				</p>
			</form>
		</div>
	</div>
</div>