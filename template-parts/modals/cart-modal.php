<div id="cart-modal" class="cart-modal page-modal">
	<div class="off-modal-layer"></div>
	<div class="cart-panel page-panel">
		<div class="modal-header cart-modal-header">
			<h3 class="title-modal">You Cart
				<span id="count-item-cart"></span>
			</h3>
			<a href="#" class="button-close">
				<span>
					<svg aria-hidden="true" role="img" focusable="false" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
						<line x1="18" y1="6" x2="6" y2="18"></line>
						<line x1="6" y1="6" x2="18" y2="18"></line>
					</svg>
				</span>
			</a>
		</div>
		<div class="modal-content cart-content">
			<div class="widget_shopping_cart_content">
				<?php woocommerce_mini_cart(); ?>
			</div>
		</div>
	</div>
</div>