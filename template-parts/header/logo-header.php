<?php
/**
 * Template part for displaying the logo
 *
 * @package Konte
 */

$logo_type = get_theme_mod( 'razzii_logo_type' );

?>
<div class="header-left-item header-item">
	<div class="logo-shop-header">
		<a href="<?php echo esc_url( home_url() ) ?>" class="site-logo">
			<?php if ( 'text' == $logo_type ) : ?>
				<span class="logo-text"><?php echo esc_html( get_theme_mod( 'razzii_logo_text' ) ) ?></span>
			<?php elseif ( 'svg' == $logo_type ) : ?>
				<span class="logo-svg"><?php echo get_theme_mod( 'razzii_logo_svg' ) ?></span>
			<?php else : ?>
				<img width="<?php echo esc_attr( get_theme_mod( 'dimension_width' ) ) ?>" height="<?php echo esc_attr( get_theme_mod( 'dimension_height' ) ) ?>" src="<?php echo esc_url( get_theme_mod( 'razzii_logo_image' ) ); ?>" />
			<?php endif; ?>
		</a>
	</div>
</div>