<?php

/**
 * Template part for displaying the topbar
 *
 * @package Razzii
 */
$topbar = get_theme_mod('razzii_topbar');
$menu_left = get_theme_mod('topbar_left_item');

?>

<?php if ('on' === $topbar) : ?>
    <div id="site-header-1-top-box" class="site-header-1-top-box" style="height : <?php echo get_theme_mod('topbar_height') ?>px;">
        <div class="site-header-1-top container">
            <div class="topbar-item-left">
                <div class="icon-free-shipping">
                    <svg width="17" height="14" viewBox="0 0 17 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.2273 9.66667V1H1V9.66667H11.2273ZM11.2273 9.66667H16V6.33333L13.9545 4.33333H11.2273V9.66667ZM5.77273 11.3333C5.77273 12.2538 5.00958 13 4.06818 13C3.12679 13 2.36364 12.2538 2.36364 11.3333C2.36364 10.4129 3.12679 9.66667 4.06818 9.66667C5.00958 9.66667 5.77273 10.4129 5.77273 11.3333ZM14.6364 11.3333C14.6364 12.2538 13.8732 13 12.9318 13C11.9904 13 11.2273 12.2538 11.2273 11.3333C11.2273 10.4129 11.9904 9.66667 12.9318 9.66667C13.8732 9.66667 14.6364 10.4129 14.6364 11.3333Z" stroke="#111111" stroke-width="1.1" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </div>
                <span class="text-free-shipping">
                    <p>Free shipping over $100</p>
            </div>
            <div class="item-topbar money-list">
                <div class="dropdown">
                    <span class="current">
                        <span class="selected">USD</span>
                        <span class="rz-icon-svg">
                            <svg aria-hidden="true" role="img" focusable="false" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                <polyline points="6 9 12 15 18 9"></polyline>
                            </svg>
                        </span>
                    </span>
                </div>

                <div class="currency-dropdown content-droplist">
                    <ul>
                        <li>
                            <a href="#">USD</a>
                        </li>
                        <li>
                            <a href="#">USD</a>
                        </li>
                        <li>
                            <a href="#">USD</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="item-topbar language-list">
                <div class="dropdown">
                    <span class="current">
                        <span class="selected">English</span>
                        <span class="rz-icon-svg">
                            <svg aria-hidden="true" role="img" focusable="false" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                <polyline points="6 9 12 15 18 9"></polyline>
                            </svg>
                        </span>
                    </span>
                </div>

                <div class="currency-dropdown content-droplist">
                    <ul>
                        <li>
                            <a href="#">English</a>
                        </li>
                        <li>
                            <a href="#">English</a>
                        </li>
                        <li>
                            <a href="#">English</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="topbar-right-item">
                <?php if ('menu_topbar' === $menu_left) : ?>
                    <div class="topbar-menu-1">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'topbar-menu',
                            'container'      => 'nav',
                            'container_class' => 'topbar-menu-container',
                            'menu_class'     => 'nav-menu topbar-menu menu',
                        ))
                        ?>
                    </div>
                <?php elseif ('menu_social' === $menu_left) : ?>
                    <div class="topbar-menu-2">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'social-topbar-menu',
                            'container' => 'nav',
                            'container_class' => 'social-menu-container',
                            'menu_class' => 'nav-menu social-menu menu',
                        ))
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>