<?php
/**
 * Displays site header
 *
 * @package WordPress
 * @subpackage Razzii
 */
?>
<header id="site-header" class="site-header">
	<div class="site-header-container header-1">
		<?php get_template_part( 'template-parts/header/topbar-header' ); ?>

		<div id="site-header-1-bottom-box" class="site-header-1-bottom-box">
			<div class="site-header-1-bottom container">
				<?php get_template_part( 'template-parts/header/logo-header' ); ?>
				<?php get_template_part( 'template-parts/header/menu-primary-header' ); ?>
				<?php get_template_part( 'template-parts/header/menu-icon-bar' ); ?>
			</div>
		</div>
	</div>
	<!-- header mobile -->
	<?php get_template_part( 'template-parts/header/site-header-mobile' ); ?>
</header>