<?php
/**
 * Displays header menu primary header
 * @package WordPress
 * @subpackage Razzii
 */
?>
<div class="header-center-item header-item">
    <?php
    wp_nav_menu( array(
        'theme_location' => 'primary',
        'container'      => 'nav',
        'container_class' => 'primary-menu-container',
        'menu_class'     => 'nav-menu primary-menu menu',
    ) )
    ?>
    <!-- <script>
        $("a").last().addClass("selected highlight");
    </script> -->
</div>
