<?php
/**
 * Displays header mobile
 *
 * @package WordPress
 * @subpackage Razzii
 */
?>
<div class="site-header-container header-2">
	<div class="site-header-2-top-box">
		<div class="site-header-2-top container">
			<div class="item-topbar money-list">
				<div class="select-option-money select-option-header">
					<div class="select-country">
						<p>USD</p>
					</div>
					<div class="icon-country-language-money">
						<svg width="7" height="4" viewBox="0 0 7 4" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path
								d="M3.15346 3.85609L0.143669 0.838849C-0.0478897 0.646917 -0.0478897 0.335731 0.143669 0.143891C0.335056 -0.0479637 0.645469 -0.0479637 0.836841 0.143891L3.50005 2.81368L6.16317 0.143969C6.35463 -0.0478861 6.66501 -0.0478861 6.8564 0.143969C7.04787 0.335824 7.04787 0.646994 6.8564 0.838927L3.84656 3.85616C3.75082 3.95209 3.62547 4 3.50007 4C3.3746 4 3.24916 3.952 3.15346 3.85609Z"
								fill="#111111" />
						</svg>
					</div>
					<div class="dropdown-money-list dropdown-list-header">
						<ul>
							<li>
								<p>USD</p>
							</li>
							<li>
								<p>USD</p>
							</li>
							<li>
								<p>USD</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="item-topbar language-list">
				<div class="select-option-language select-option-header">
					<div class="select-country">
						<p>English</p>
					</div>
					<div class="icon-country-language-money">
						<svg width="7" height="4" viewBox="0 0 7 4" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path
								d="M3.15346 3.85609L0.143669 0.838849C-0.0478897 0.646917 -0.0478897 0.335731 0.143669 0.143891C0.335056 -0.0479637 0.645469 -0.0479637 0.836841 0.143891L3.50005 2.81368L6.16317 0.143969C6.35463 -0.0478861 6.66501 -0.0478861 6.8564 0.143969C7.04787 0.335824 7.04787 0.646994 6.8564 0.838927L3.84656 3.85616C3.75082 3.95209 3.62547 4 3.50007 4C3.3746 4 3.24916 3.952 3.15346 3.85609Z"
								fill="#111111" />
						</svg>
					</div>
					<div class=" dropdown-language-list dropdown-list-header">
						<ul>
							<li>
								<p>English</p>
							</li>
							<li>
								<p>Lao</p>
							</li>
							<li>
								<p>Vietnam</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="site-header-2-bottom-box" class="site-header-2-bottom-box">
		<div class="site-header-2-bottom container">
			<div class="menu-icon-header">
				<svg class="icon-menu-mobile" aria-hidden="true" role="img" focusable="false" width="24" height="18"
					viewBox="0 0 24 18" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					<path d="M24 0H0V2.10674H24V0Z"></path>
					<path d="M24 7.88215H0V9.98889H24V7.88215Z"></path>
					<path d="M24 15.8925H0V18H24V15.8925Z"></path>
				</svg>
			</div>
			<div class="logo-shop-header">
				<a href="<?php echo site_url( '/home' ); ?>" style="text-decoration: none; color: black;">
					<h1>Razzi.</h1>
				</a>
			</div>
			<div class="menu-header-icon-bar">
				<div class="menu-header-icon-bar-list">
					<svg width="20" height="17" viewBox="0 0 20 17" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path
							d="M17.612 2.32611C17.1722 1.90569 16.65 1.57219 16.0752 1.34465C15.5005 1.11711 14.8844 1 14.2623 1C13.6401 1 13.0241 1.11711 12.4493 1.34465C11.8746 1.57219 11.3524 1.90569 10.9126 2.32611L9.99977 3.19821L9.08699 2.32611C8.19858 1.4773 6.99364 1.00044 5.73725 1.00044C4.48085 1.00044 3.27591 1.4773 2.38751 2.32611C1.4991 3.17492 1 4.32616 1 5.52656C1 6.72696 1.4991 7.87819 2.38751 8.727L3.30029 9.5991L9.99977 16L16.6992 9.5991L17.612 8.727C18.0521 8.30679 18.4011 7.80785 18.6393 7.25871C18.8774 6.70957 19 6.12097 19 5.52656C19 4.93214 18.8774 4.34355 18.6393 3.7944C18.4011 3.24526 18.0521 2.74633 17.612 2.32611V2.32611Z"
							stroke="#111111" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
					</svg>
					<svg class="icon-cart" width="21" height="20" viewBox="0 0 21 20" fill="none"
						xmlns="http://www.w3.org/2000/svg">
						<path
							d="M8.77273 18.1429C8.77273 18.6162 8.38606 19 7.90909 19M8.77273 18.1429C8.77273 17.6695 8.38606 17.2857 7.90909 17.2857M8.77273 18.1429H7.04545M7.90909 19C7.43212 19 7.04545 18.6162 7.04545 18.1429M7.90909 19V18.1429C7.90909 18.4206 7.90909 18.6379 7.90909 17.2857M7.04545 18.1429C7.04545 17.6695 7.43212 17.2857 7.90909 17.2857M18.2727 18.1429C18.2727 18.6162 17.8861 19 17.4091 19M18.2727 18.1429C18.2727 17.6695 17.8861 17.2857 17.4091 17.2857M18.2727 18.1429H16.5455M17.4091 19C16.9321 19 16.5455 18.6162 16.5455 18.1429M17.4091 19V17.2857M16.5455 18.1429C16.5455 17.6695 16.9321 17.2857 17.4091 17.2857M1 1H4.45455L6.76909 12.4771C6.84807 12.8718 7.06438 13.2263 7.38015 13.4785C7.69593 13.7308 8.09106 13.8649 8.49636 13.8571H16.8909C17.2962 13.8649 17.6913 13.7308 18.0071 13.4785C18.3229 13.2263 18.5392 12.8718 18.6182 12.4771L20 5.28571H5.31818"
							stroke="#111111" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
					</svg>
				</div>
			</div>
		</div>
	</div>
</div>