<?php

/**
 * Template part for displaying single post content
 *
 * @package Razzii
 */

?>
<header class="header-single">
	<h1 class="entry-title"><?php the_title(); ?></h1>
	<div class="author-date-post">By <?php echo get_the_author(); ?> / <?php the_time('M d',) ?>
	</div>
	<div class="post-thumbnail">
		<?php if (has_post_thumbnail()) : ?>
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
		<?php endif; ?>
	</div>

</header>

<div class="content-post-container">
	<?php the_content(); ?>
	<div class="hashtag-social-post">
		<div class="hashtag-keyword-post">
			<?php
			$tags = get_terms('post_tag');
			echo '<div class="rz-post-tag">';
			foreach ($tags as $tag) {
				echo '<a href="#">' . $tag->name . '</a>';
			}
			echo '</div>';
			?>
		</div>
		<div class="social-share-post">
			<a href="https://www.facebook.com/sharer.php?u=https://demo4.drfuri.com/razzi/heaven-upon-heaven-moveth-every-have/" target="_blank" class="social-share-link facebook"><span class="rz-icon-svg "><svg aria-hidden="true" role="img" focusable="false" width="24" height="24" viewBox="0 0 7 12" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						<path d="M5.27972 1.99219H6.30215V0.084375C6.12609 0.0585937 5.51942 0 4.81306 0C3.33882 0 2.32912 0.99375 2.32912 2.81953V4.5H0.702148V6.63281H2.32912V12H4.32306V6.63281H5.88427L6.13245 4.5H4.32306V3.03047C4.32306 2.41406 4.47791 1.99219 5.27972 1.99219Z"></path>
					</svg></span><span class="after-text">Share on Facebook</span></a>
			<a href="https://twitter.com/intent/tweet?url=https://demo4.drfuri.com/razzi/heaven-upon-heaven-moveth-every-have/&amp;text=Heaven%20upon%20heaven%20moveth%20every%20have." target="_blank" class="social-share-link twitter"><span class="rz-icon-svg "><svg aria-hidden="true" role="img" focusable="false" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
						<path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path>
					</svg></span><span class="after-text">Share on Twitter</span></a>
			<a href="https://plus.google.com/share?url=https://demo4.drfuri.com/razzi/heaven-upon-heaven-moveth-every-have/" target="_blank" class="social-share-link googleplus"><span class="rz-icon-svg "><svg aria-hidden="true" role="img" focusable="false" width="24" height="24" fill="currentColor" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg">
						<path d="M12.02,10.18v3.72v0.01h5.51c-0.26,1.57-1.67,4.22-5.5,4.22c-3.31,0-6.01-2.75-6.01-6.12s2.7-6.12,6.01-6.12 c1.87,0,3.13,0.8,3.85,1.48l2.84-2.76C16.99,2.99,14.73,2,12.03,2c-5.52,0-10,4.48-10,10s4.48,10,10,10c5.77,0,9.6-4.06,9.6-9.77 c0-0.83-0.11-1.42-0.25-2.05H12.02z"></path>
					</svg></span><span class="after-text">Share on Google+</span></a>
			<a href="https://plus.google.com/share?url=https://demo4.drfuri.com/razzi/heaven-upon-heaven-moveth-every-have/" target="_blank" class="social-share-link googleplus"><span class="rz-icon-svg "><svg aria-hidden="true" role="img" focusable="false" width="24" height="24" viewBox="0 0 7 12" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						<path d="M5.27972 1.99219H6.30215V0.084375C6.12609 0.0585937 5.51942 0 4.81306 0C3.33882 0 2.32912 0.99375 2.32912 2.81953V4.5H0.702148V6.63281H2.32912V12H4.32306V6.63281H5.88427L6.13245 4.5H4.32306V3.03047C4.32306 2.41406 4.47791 1.99219 5.27972 1.99219Z"></path>
					</svg></span><span class="after-text">Share on Instagram</span></a>
		</div>

	</div>
</div>
<div class="rz-related-post">
	<?php get_template_part('template-parts/posts/related-posts'); ?>
</div>