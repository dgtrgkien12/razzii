<?php

/**
 * Template part for displaying posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Razzii
 */

?>

<div class="post-page-container">
	
	<div class="post-page-col-list">
		<?php

		while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" class="col-post-page">
				<div class="post">
					<div class=entry-header>
						<div class="date-post">
							<p><?php the_time('M d',) ?></p>
						</div>
						<?php if (has_post_thumbnail()) : ?>
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
						<?php endif; ?>
					</div>
					<div class="entry-summary">
						<h4><a href="<?php the_permalink(); ?>" style="text-decoration: none;"><?php echo the_title(); ?></a></h4>
						<div class="entry-content">
							<p><?php echo wp_trim_words(get_the_content(), 23) ?></p>
						</div>
						<a class="button-normal" href="<?php the_permalink(); ?>">Read more
							<span class="rz-icon-svg"><svg class="image-post" aria-hidden="true" role="img" focusable="false" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
									<line x1="0" y1="12" x2="19" y2="12"></line>
									<polyline points="12 5 19 12 12 19"></polyline>
								</svg></span>
						</a>

					</div>
				</div>

			</div>
		<?php endwhile; ?>
	</div>

	<div class="page-post-found">
		<div class="post-inner">Show
			<span></span> of
			<span></span> page
			<span class="count-bar" width=""></span>
		</div>

	</div>

	<nav id="load-navigation" class="load-navigation">
		<div class="nav-links">
			<?php if (get_next_posts_link()) : ?>
				<div id="blog-previous-ajax" class="nav-previous-ajax">
					<?php next_posts_link('Load More'); ?>
				</div>
			<?php endif; ?>
		</div>
	</nav>

</div>