<?php

/**
 * Template part for displaying posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Razzii
 */
?>

<div class="page-post-found">
	<div class="post-inner">Show
		<span></span> of
		<span></span> page
		<span class="count-bar" width=""></span>
	</div>

</div>
<nav class="load-navigation load-product">
	<div class="nav-links">
		<?php if (get_next_posts_link()) : ?>
			<div id="product-previous-ajax" class="nav-previous-ajax">
				<?php next_posts_link('Load More'); ?>
			</div>
		<?php endif; ?>
	</div>
</nav>