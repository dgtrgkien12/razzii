<?php

/**
 * Template part for displaying related posts
 *
 * @package Razzii
 */
// $do_not_duplicate = $post->ID;
$related_post = new WP_Query(array(
	'orderby'        => 'rand',
	'post__not_in'   => array(get_the_ID()),
	'category__in'           => wp_get_post_categories(),
	'posts_per_page' => 3,
));

?>
<div class="related-posts">
	<h3 class="title-related-posts">Related Posts</h3>
	<div class="related-posts-list">
		<div class="related-posts-content-list">

			<?php while ($related_post->have_posts()) : $related_post->the_post(); ?>
				<div class="post">
					<div class=entry-header>
						<div class="date-post">
							<p><?php the_time('M d',) ?></p>
						</div>
						<?php if (has_post_thumbnail()) : ?>
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
						<?php endif; ?>
					</div>
					<div class="entry-summary">
						<h4><a href="<?php the_permalink(); ?>" style="text-decoration: none;"><?php echo the_title(); ?></a></h4>
					</div>
				</div>

			<?php endwhile; ?>
		</div>
	</div>
</div>