<ul class="list-image swiper-wrapper">
	<li class="swiper-slide">
		<a href="#">
			<img src="<?php echo get_theme_file_uri( 'assets/images/catalog_banner1.jpg' ) ?>" alt="">
		</a>
	</li>
	<li class="swiper-slide">
		<a href="#">
			<img src="<?php echo get_theme_file_uri( 'assets/images/catalog_banner2.jpg' ) ?>" alt="">
		</a>
	</li>
</ul>
<div class="swiper-pagination"></div>